# A simple python program to convert Celcius to Farenheight
# Usage: python c2f.py 25.0
# Authors: Kawsar (gitlab.com/kawsark)
import re
import sys

if len(sys.argv) > 1:
    match = re.search("[-0-9\.]+", sys.argv[1])
    if not match:
        match = re.search("[-0-9]+", sys.argv[1])

    if match:
        c = match.group(0)
        f = (float(c) * 9 / 5) + 32
        print("(%s, %f)" %(c,f))
    else:
        print ("(%s, -1)" % sys.argv[1])

else:
    print ("(,)")
