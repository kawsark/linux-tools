#!/bin/bash

# A simple bash script to upload system metadata to Consul KV store - loops every 10 mins by default.
# Relies on python/c2f.py and python/therm.py scripts
# Authors: Kawsar (gitlab.com/kawsark)

interval=600
therm=false
while true
do
    echo "[System monitor] - writing result"
    # Get temperature from thermometer
    [[ $therm = "true" ]] \
    && m=$(python /usr/local/bin/therm.py) \
	&& echo $m \
	&& consul kv put system/$HOSTNAME/therm "$m"

    # Get system temperature
    t=$(/opt/vc/bin/vcgencmd measure_temp) \
	&& m=$(python /usr/local/bin/c2f.py $t) \
	&& echo $m \
	&& consul kv put system/$HOSTNAME/system_temp "$m"

    # Put updated timestamp
    d=$(date '+%Y.%m.%d-%H:%M:%S') && consul kv put system/$HOSTNAME/updated "$d"

    sleep $interval
done
