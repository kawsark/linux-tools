#!/bin/bash
#app=java
#start_cmd=minecraft-launcher
workspace=/tmp
block=300
allowed_blocks=5
max_time=2300
consul_members=0

set_allowed_blocks () {
  consul_members=0
  consul_members=$(consul members | grep alive | wc | awk '{print $1}')
  if [[ ${consul_members} -ge '2' ]] && [[ ! -z ${user} ]]
  then
    export allowed_blocks=$(consul kv get play/${user}/allowed_blocks)
  else
    # Check if there an Environment variable set for Allowed blocks
    [[ ! -z ${PLAY_ALLOWED_BLOCKS} ]] && export allowed_blocks=${PLAY_ALLOWED_BLOCKS} || export allowed_blocks=5
  fi
}

cd $workspace
if [ ! -z $(pgrep $app) ];then
  pkill $app
  echo "Waiting 10 seconds for app to exit"
  sleep 10
fi

# Check time
d=$(date +%H%M)
if [ ${d} -gt ${max_time} ];then
  echo "Sorry, it is past the allowed time"
  exit 1
fi

echo "Enter username:"
read user
echo "Enter password:"
read password

check_pw=$(cat "$HOME/.${user}_password")
if [[ $password != $check_pw ]]
then
    echo "Sorry, incorrect Username or Password"
    exit 1
fi

# Ask for game
echo "What do you want to play? m = minecraft, b = baldi"
read game
if [[ $game == "m" ]]
then
  echo "Enjoy Minecraft"
  export app=java
  export start_cmd=minecraft-launcher
elif [[ $game = "b" ]]
then 
  echo "Enjoy BALDI!"
  export app=BALDI
  export start_cmd=steam
else
  echo "Please choose m or b. Byeeeeeee"
  exit 1
fi

# Check qty. of blocks
set_allowed_blocks
qty_of_blocks=$(ls -l $user-$(date +%y%m%d)* | wc -l | awk '{print $1}' 2>/dev/null) 2>/dev/null
if [ ! -z $qty_of_blocks ]; then
  if (( $qty_of_blocks >= $allowed_blocks )); then
    echo "Found $qty_of_blocks time blocks for today"
    echo "Sorry $user, you have used up all your allowed time today."
    exit 0
  else
    echo "Staring app"
    $start_cmd &
  fi
fi

# Check qty. of blocks
echo "Clean up old files from last 3 days"
for (( i=1;i<4;i++ ));do
  rm -f *$(date --date "-${i} days" +%y%m%d)*
done

# Check apply status periodically in loop
continue=1
while [ $continue -ne 0 ]; do
  sleep $block
  echo "Loop check @ $(date +%y%m%d%H%M%S)"

  if [ ! -z $(pgrep $app) ]; then
    qty_of_blocks=$(ls -l $user-$(date +%y%m%d)* | wc -l | awk '{print $1}')
    if [ ! -z $qty_of_blocks ];then
  set_allowed_blocks
	let "remaining_blocks = $allowed_blocks - $qty_of_blocks"
	let "remaining_mins = ($remaining_blocks * $block)/60"
        if (( $qty_of_blocks > $allowed_blocks )); then
          echo "Sorry $user, you have used up all your allowed time today."
          notify-send "Please save game and exit now, killing game in 60 seconds."
	  sleep 60
	  pkill $app
	  echo "Waiting 60 seconds before forcing."
	  sleep 60
	  pkill -9 $app
	else
	    message="You have ${remaining_mins} Minutes left."
	    [[ $remaining_blocks == 1 ]] && notify-send $message || echo $message
        fi
      fi
  else
    echo "Child process has exited"
    continue=0
  fi

  if [ $continue -ne 0 ]; then
    touch $user-$(date +%y%m%d%H%M%S)
  fi

done

