#!/bin/bash

# This script uses the cfcli utility to add _acme-challenge TXT records to CloudFlare

usage="usage: renewcertCF.sh <domain> <TXT>"
source setupcf.sh
domain=$1
txt=$2

# Ask for the Domain if needed
if [[ -z ${domain} ]]; then
  echo "Note: this script can be used non-interactively: ${usage}"
  echo "Enter the Domain name you want to add as _acme-challenge.<domain_name>"
  read domain
  [[ -z ${domain} ]] && echo "${usage}" && exit
fi

# Ask for the TXT record if needed
if [[ -z ${txt} ]]; then 
  echo "Note: this script can be used non-interactively: ${usage}"
  echo "Enter the TXT record you want to add for _acme-challenge.${domain}"
  read txt
  [[ -z ${txt} ]] && echo "${usage}" && exit 
fi

cfcli -d ${domain} ls
cfcli -d ${domain} rm _acme-challenge
cfcli -d ${domain} add -t TXT _acme-challenge ${txt}
cfcli -d ${domain} ls
