#!/bin/bash

echo "Enter username:"
read user
echo "Enter password:"
read password

check_pw=$(cat "$HOME/.${user}_password")
if [[ $password != $check_pw ]]
then
    echo "Sorry, incorrect Username or Password"
    exit 1
else
    echo "Enter new password"
    read pw1
    echo "Enter new password again"
    read pw2
    if [[ $pw1 == $pw2 ]]; then
        echo $pw1 > "$HOME/.${user}_password"
    else
        echo "Sorry passwords don't match"
    fi
fi