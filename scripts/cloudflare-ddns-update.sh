#!/bin/bash

# A bash script to update CloudFlare DNS - loops every 10 mins by default.
# Authors: Kawsar (gitlab.com/kawsark), https://github.com/Tras2
# Usage: 
## export CF_ZONE=<e.g. example.com>
## export CF_DNSRECORD=<e.g. www>
## export CF_DNSRECORD2=<optional second record to update>
## export CF_API_EMAIL=
## export CF_API_KEY=
## cloudflare-ddns-update.sh

interval=600
result=true
retry=0
function update_dns() {

    # Get the current external IP address
    ip=$(curl -s -X GET https://checkip.amazonaws.com)
    #ip=$(curl -s http://whatismyip.akamai.com)

    echo "[CloudFlare DNS Updater] - Current IP is $ip"

    # Check the current IP and match against DNS result
    if host $CF_DNSRECORD 1.1.1.1 | grep "has address" | grep "$ip"; then
	    echo "[CloudFlare DNS Updater] - $CF_DNSRECORD is currently set to $ip; no changes needed"
	    result=true
      return 0
    fi

    # if here, the dns record needs updating

    # get the zone id for the requested zone
    zoneid=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$CF_ZONE&status=active" \
      -H "X-Auth-Email: $CF_API_EMAIL" \
      -H "X-Auth-Key: $CF_API_KEY" \
      -H "Content-Type: application/json" | jq -r '{"result"}[] | .[0] | .id')

    echo "[CloudFlare DNS Updater] - Zoneid for $CF_ZONE is $zoneid"

    # get the dns record id
    dnsrecordid=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records?type=A&name=$CF_DNSRECORD" \
      -H "X-Auth-Email: $CF_API_EMAIL" \
      -H "X-Auth-Key: $CF_API_KEY" \
      -H "Content-Type: application/json" | jq -r '{"result"}[] | .[0] | .id')

    echo "[CloudFlare DNS Updater] - DNSrecordid for $CF_DNSRECORD is $dnsrecordid"

    # update the record
    response=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records/$dnsrecordid" \
      -H "X-Auth-Email: $CF_API_EMAIL" \
      -H "X-Auth-Key: $CF_API_KEY" \
      -H "Content-Type: application/json" \
      --data "{\"type\":\"A\",\"name\":\"$CF_DNSRECORD\",\"content\":\"$ip\",\"ttl\":1,\"proxied\":false}")

    # Print out the response
    echo "[CloudFlare DNS Updater] - response is $response"

    # set the result
    result=$(echo $response | jq -r .success)
    echo "[CloudFlare DNS Updater] - result is $result"

    # Check if CF_DNSRECORD2 is set and update that
    if  [[ ! -z $CF_DNSRECORD2 ]]; then

      echo "[CloudFlare DNS Updater] - Updating DNS record for $CF_DNSRECORD2"

	    # get the dns record id
      dnsrecordid2=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records?type=A&name=$CF_DNSRECORD2" \
        -H "X-Auth-Email: $CF_API_EMAIL" \
        -H "X-Auth-Key: $CF_API_KEY" \
        -H "Content-Type: application/json" | jq -r '{"result"}[] | .[0] | .id')

      echo "[CloudFlare DNS Updater] - DNSrecordid for $CF_DNSRECORD2 is $dnsrecordid"

      # update the record
      response2=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records/$dnsrecordid2" \
        -H "X-Auth-Email: $CF_API_EMAIL" \
        -H "X-Auth-Key: $CF_API_KEY" \
        -H "Content-Type: application/json" \
        --data "{\"type\":\"A\",\"name\":\"$CF_DNSRECORD2\",\"content\":\"$ip\",\"ttl\":1,\"proxied\":false}")

      # Print out the response
      echo "[CloudFlare DNS Updater] - response is $response2"
    fi

}

while true
do
    update_dns
    echo "[CloudFlare DNS Updater] - examining result"
    if [ $result != "true" ]; then
        if [ $retry == 3 ]; then
	    echo "[CloudFlare DNS Updater] - Received a non-successful response $retry times, exiting"
            break
        else
            let "retry=retry+1"
            echo "[CloudFlare DNS Updater] - Received a non-successful response. Retry: $retry after $interval seconds"
	fi
    else
	echo "[CloudFlare DNS Updater] - going to sleep for $interval seconds"
	retry=0
    fi
    sleep $interval
done
